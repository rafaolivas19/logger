# Logger

Este proyecto consiste en un flujo de Node-RED que contiene una serie de APIs que permitirán el registro de logs en un sistema.

## Puesta en marcha

### Requisitos

1. Node.js instalado (https://nodejs.org/, se puede omitir si se pretende usar Docker).
1. Node-RED (https://nodered.org/#get-started).
1. Habilita moment.js en Node-RED (ver [Habilitar moment.js en Node-RED](/logger/3_instrucciones_adicionales/#habilitar-momentjs-en-node-red)).
1. Agrega el nodo `json-full-schema-validator` ([aquí](https://nodered.org/docs/user-guide/runtime/adding-nodes) dice como agregar nodos).

### Instalación

1. Importa el flujo a Node-RED.
1. En la sección de configuración del flujo indica:
    1. El directorio donde se guardarán los logs (si no se indica será el directorio HOME del usuario por defecto).
1. Ir al directorio de instalación de Node-RED (normalmente `~/.node-red/`).
1. Ejecutar el comando `npm install moment --save` para instalar el paquete de moment.js (desde el mismo directorio).
1. Editar el archivo `settings.js` agrega moment.js al contexto global del siguiente modo (busca la sección `functionGlobalContext`, este paso es muy importante ya que el flujo hace uso del módulo `moment` de npm y es así como podrá acceder a el):

    ``` js
    functionGlobalContext: {
        // os:require('os'),
        // jfive:require("johnny-five"),
        // j5board:require("johnny-five").Board({repl:false}),
        moment: require('moment')
    },
    ```

1. Reinicia Node-RED.
1. Listo. Revisa [la documentación de las APIs](/logger/2_rest_apis/) para que te des una mejor idea de como usar el flujo.