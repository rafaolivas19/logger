# REST APIs

El flujo de Node-RED incluye los siguintes REST APIs para ser consumidos.

## POST Init

- Este API debe ser llamado antes que cualquier otro. 
- Mediante este se indica el nombre de la aplicación o instancia que estará registrando logs.
- Generará un token y lo devolverá en su respuesta, este debe ser usado en el resto de los APIs para poder ejecutarlas.

| Método | Endpoint          |
|--------|-------------------|
| `POST` | `/logger/v1/init` |

### Petición

``` json
{
    "name": "My App Name"
}
```

### Respuesta

``` json
{
    "token": "a29c41e7-0097-4799-b109-fcab7eb8b45c",
    "status": "ok"
}
```

### Notas

- Si este API es invocado y ya existe un token, se sustituirá el viejo token con uno nuevo y el anterior ya no será válido.
- El llamado a este API provocará que se registre en el archivo log un mensaje como el siguiente:

``` log
■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
LOGGER INIT [2020-06-20 15:29:34.6489-07:00]
■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
```

## POST Log

- Registra un mensaje en el archivo log.
- Debe indicarse el token recibido por el API [POST Init](#post-init).

| Método | Endpoint                       |
|--------|--------------------------------|
| `POST` | `/logger/v1/log?token={TOKEN}` |

### Petición

``` json
{
    "type": "INFO", // opcional, INFO por defecto, solo se permiten los valores INFO, WARNING y ERROR
    "header": "LOG", // opcional
    "message": "This is a log entry"
}
```

### Respuesta

``` json
{
    "status": "ok"
}
```

### Notas

- El ejemplo de arriba registrará lo siguiente en el archivo log: `[21:07:57.6400-07:00] [INFO] This is a log entry`
- Puede responder con `invalid_token` si no se indica un token o el token indicado no es válido.

